#!/usr/bin/env python

#Created      Oct2, 2019 --Jiayi Chen, jennyz@brandeis.edu
#last update: Oct2, 2019 --Jiayi

import io, argparse, json, ast, os, time, collections
#from change_input_VBFNLO import changeParameter

#change parameter in the given file_name from the input path and dump it to the output path
def changeParameter(input_path = './', output_path = './',
                file_name = 'cuts.dat', name = '', value = ''):

    #input_path = args.input_path
    print(input_path)
    if input_path[-1] != '/':
        input_path+="/"
    input_path+=file_name

    output_path =output_path + file_name

    print("openning input file: ", input_path)

    with open(input_path, 'r') as file:
        # read a list of lines into data
        lines = file.readlines()

    #a flag to confirm the parameter name is correct
    name_is_found = False

    #look for the given parameter
    for ind, line in enumerate(lines):
        if name in line:

            lineno = ind
            name_is_found = True
            start_ind = line.index('=')

            end_ind = -1
            #keep the comments starting with !
            if "!" in line:
                end_ind = line.index('!')

            #create new line, inserting new value
            new_line = line[:start_ind] + "=" + value + line[end_ind:]
            break

    if name_is_found:
        print("new parameter:", new_line)

        #replace parameter value
        lines[lineno] = new_line
        with open(output_path, 'w') as file:
            file.writelines( lines )
        print("new file saved to ", output_path)

    else:
        print("did not find the given parameter name: ", name)

def main(datFiles_input_path = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/myinput/VBFNLO/",new_dir = "",path_to_grid = ""):
    #full path to new_parameters.json
    parameter_lists = new_dir+"new_parameters.json"
    #path_to_LOgrid = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/VBFNLO_Analysis/NLO_pts-31/grid2_1.out.4 "
    path_to_LOgrid = None


    #read json
    with open(parameter_lists, 'r') as file:
        #read a list of lines into data
        parameters = json.load(file)

    #translate json to dict
    parameters = ast.literal_eval(json.dumps(parameters))

    #origin of the default .dat files
    previous_dir = datFiles_input_path

    #go to the new directory, need to run VBFNLO here
    os.chdir(new_dir)

    #path to copy .dat files from
    vbfnlo_in_path  = previous_dir+"vbfnlo.dat"  #
    anomV_in_path   = previous_dir+"anomV.dat"   # change to
    anomHVV_in_path = previous_dir+"anom_HVV.dat"# a for loop maybe?
    cuts_in_path    = previous_dir+"cuts.dat"    #

    #copy files from the previous directory to new directory
    print "INFO: copying vbfnlo.dat, cuts.dat, anomHVV.dat, anomV.dat"
    os.system("cp "+vbfnlo_in_path + " "+new_dir)
    os.system("cp "+anomV_in_path  + " "+new_dir)
    os.system("cp "+anomHVV_in_path+ " "+new_dir)
    os.system("cp "+cuts_in_path   + " "+new_dir)

    #optimized grids
    if path_to_LOgrid != None:
        os.system("cp "+path_to_LOgrid + " " + new_dir+"grid2_1")
    os.system("cp "+ path_to_grid + " " + new_dir+"grid3_1")

    #start to change all parameters in input cards
    for file_name in parameters.keys():
        print("=====change parameters in ", file_name, "=====")

        #iterate throu dict: 2. parameter need to be changed in this .dat file
        for name in parameters[file_name].keys():

            #get the new value for this parameter
            value = parameters[file_name][name]

            #change parameter in current param card
            changeParameter(input_path = new_dir, output_path = new_dir, file_name = file_name, name = name, value = value)
    ###changed all parameters####
    print "INFO: finished changing all parameters"

    start=time.time()
    #run VBFNLO at current dir
    #os.system("/direct/usatlas+u/jennyz/VBFNLO/VBFNLO-2.7.1/bin/vbfnlo --input=. >> output.log")
    end = time.time()
    run_time = end-start

    #time how long vbfnlo ran
    with open("timer.txt", 'w') as file:
        # read a list of lines into data
        file.write(str(run_time))


if __name__ =='__main__':
    parser=argparse.ArgumentParser(description='Change parameter in input .dat files of VBFNLO and run VBFNLO')
    #parser.add_argument('-a', '--action',type=str, default = "", help='an action to be inserted in directory name')
    parser.add_argument('--dat-input-path', type=str, default = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/myinput/VBFNLO/",
                        help='default .dat file import path')
    parser.add_argument('--new-dir', type=str, help='new directory where to save output')
    parser.add_argument('--path-to-grid', type=str, default = "", help='full path to the good grid')
    args=parser.parse_args()

    try:
        main(datFiles_input_path=args.dat_input_path, new_dir = args.new_dir,path_to_grid = args.path_to_grid)
    except KeyboardInterrupt:
        print('')
        ERROR('Exectution terminated.')
        INFO('Finished with error.')
        exit()
