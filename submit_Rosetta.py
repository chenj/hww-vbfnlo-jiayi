#!/usr/bin/env python

#Created      Sep19, 2019 --Jiayi Chen, jennyz@brandeis.edu
#last update: Sep30, 2019 --Jiayi

import io, argparse, json, ast, os, time
#from change_input_VBFNLO import changeParameter

#change parameter in the given file_name from the input path and dump it to the output path
def changeAnomCoupParam(input_path = './', output_path = './',
                file_name = 'HISZ_myinput.dat', name = '', value = ''):

    output_path =output_path + file_name

    #look for the given parameter
    lines, lineno, current_value = FindParamValue(input_path,file_name,name)


    if lineno != None:
        #create new line, inserting new value
        value = float(value)
        if value>0:
            value="+"+str(value)
        new_line = lines[lineno].replace(current_value,value)#line[:start_ind] + value + line[end_ind:]
        print("new parameter:", new_line)

        #replace parameter value
        lines[lineno] = new_line
        with open(output_path, 'w') as file:
            file.writelines( lines )
        print("new file saved to ", output_path)

    else:
        print("did not find the given parameter name: ", name)

#given the parameter name, find parameter value in the .dat file
#can be used (1)to change the parameter value (2)read output .dat after translation
def FindParamValue(input_path = './', file_name = 'HISZ_myinput.dat', name = ''):

    print(input_path)
    if input_path[-1] != '/':
        input_path+="/"
    input_path+=file_name

    print("openning input file: ", input_path)

    with open(input_path, 'r') as file:
        # read a list of lines into data
        lines = file.readlines()

    #parameter name has the comment sign in front
    name = "# "+name

    #initiate a flag
    name_is_found = False

    for ind, line in enumerate(lines):
        if name in line:
            lineno = ind
            name_is_found = True
            end_ind = line.index('#')

            #start from +/- sign
            try:
                start_ind = line.index('+')
            #if the value is negative
            except ValueError:
                start_ind = line.index('-')

            value = line[start_ind:end_ind]
            break

    #give lineno and value None value
    if not name_is_found:
        lineno = None
        value = None

    #return all the lines, the lineno of where the parameter is, the current value
    return lines, ind, value



def main(input_card, target_basis = "HISZ"):
    parameter_lists = "./RosettaParameters.json"

    #origin of the default cards
    default_param_path = "./Rosetta/Cards/"

    #read json
    with open(parameter_lists, 'r') as file:
        #read a list of lines into data
        list_of_parameters = json.load(file)

    #translate json to dict
    list_of_parameters = ast.literal_eval(json.dumps(list_of_parameters))

# a for loop of changing parameter one at a time
    #for name in list_of_parameters.keys():
    #    changeAnomCouplingParam()
    #run Rosetta

if __name__ =='__main__':
    changeAnomCoupParam(input_path = 'Rosetta/Cards/', output_path = './', file_name = 'HISZ_myinput.dat', name = 'fW', value = '888.0')
