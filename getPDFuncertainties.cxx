#include "TFile.h"
#include "TTree.h"
#include "TLatex.h"
#include "TLegend.h"
#include <iostream>
#include <algorithm>

vector<Double_t> Hessian_uncertainty(vector<Double_t>,bool);
Double_t MCreplica_uncertainty(vector<Double_t>);
vector<Double_t> inclusive_xsection(TH1D*);
TH1D* DrawOverflow(TH1D*);

void getPDFuncertainties( string output_name, bool isMCreplica = false, bool asym_error = true, bool scale90to68 = false, bool add_alpha_s_error = false)
{
  using namespace std;

  // list of histogram names
  vector<string> hnames = { //jets -- energy
                           "H_pTjj", // di-jet system pT
                           "H_mjj",
                           "H_HTjj",
                           "H_pTj1",
                           "H_pTj2",
                           //"H_pTj_min",
                           //jets -- angles
                           "H_phij1",
                           "H_phij2",
                           "H_phijj",
                           "H_etajj",
                           //number of jets
                           "H_Number_of_j",
                           //"H_dw",
                           //leptons -- energy
                           "H_pTl1",
                           "H_pTl2",
                           "H_phil1",
                           "H_phil2",
                           "H_mll",
                           "H_pTllnnu",
                           //leptons -- angles
                           "H_phill"
                           };

  ////////get NLO histogram names/////////
   vector<string> gethnames;
   int num_hnames = hnames.size();
   for (int h=0; h<num_hnames; h++){
     string hname_NLO = hnames[h]+"_NLO";
     gethnames.push_back(hname_NLO);
   }
   hnames = gethnames;

   //
   //vector<string> result_folders;
   string result_folder_prefix = "../TH1D-NLOpts-27";
   //"../CT14nlo/CT14nlo-PDF-";
   //"../NNPDF30/NNPDF30-as0118-";
   //"../mjj1000-";
   //"../NNPDF30/NNPDF30-as0118-mjj1800-";
   //"../mjj2400-";
   //../e_mu_output/try_LO-";//../NNPDF30/NNPDF30-as0118-";//
   //

   //folders come with common prefix
   //string result_folder_prefix = "../MMHT14/MMHT14-";

   //number of all member sets (including central value)
   int number_of_PDFsets = 1; //CT14nlo=57; MMHT=51; NNPDF=101

  gROOT->SetBatch(kTRUE);
  TCanvas* can = new TCanvas();

  //open a pdf file and print the canvas in the pdf
  string save = output_name + ".pdf" ; //hname + "_" +
  string save_begin = save+"(";
  string save_end = save+")";
  can->Print(save_begin.c_str(),"pdf");

  /////add alpha_s error sets root file path /////
  //string as0117_file_name = "../NNPDF30/NNPDF30as-NNPDF30_nlo_as_0117/histograms.root";
  //string as0119_file_name = "../NNPDF30/NNPDF30as-NNPDF30_nlo_as_0119/histograms.root";
  string as0117_file_name = "../CT14nlo/CT14nlo-PDF-CT14nlo_as_0117-muR-muF/histograms.root";
  string as0119_file_name = "../CT14nlo/CT14nlo-PDF-CT14nlo_as_0119-muR-muF/histograms.root";
  //get alpha_s sets root files
  TFile *as0117_file = new TFile(as0117_file_name.c_str());
  TFile *as0119_file = new TFile(as0119_file_name.c_str());
  ////////////////////////////////

  //find uncertainty for each type of histogram
  for(auto hname : hnames)
  {
    cout << "#### ======================= ###" << '\n';
    cout << hname << '\n';

    //get all the histograms.root TFiles
    TFile *files[number_of_PDFsets];

    //j is for the case when some error sets outcome has some problem, need to use some subsets
    int j = 0;

   for(int i=0; i<number_of_PDFsets; i++)
   {
     //if (i == 15 ||i == 16 || i == 17|| i == 18 || i == 19|| i == 20) continue;

     //string file_name =  result_folder_prefix + to_string(i) + "/histograms.root";
     string file_name =  result_folder_prefix + "/histograms.root";
     files[j] = new TFile(file_name.c_str());
     j++;
   }

    //initiate the hists list
    TH1D** hists = new TH1D*[number_of_PDFsets];

    //initiate hists for alpha_s sets
    TH1D* hist_as0117;
    TH1D* hist_as0119;

    //fill in the list: hists, with same type but from different PDF sets
    for(int i=0; i<number_of_PDFsets; i++)
    {
      hists[i] = dynamic_cast<TH1D*> (files[i]->Get(hname.c_str()));
      hists[i]->Sumw2();
    }

    //get this type of hist from the alpha_s sets
    if (add_alpha_s_error)
    {
      hist_as0117 = dynamic_cast<TH1D*> (as0117_file->Get(hname.c_str()));
      hist_as0117->Sumw2();

      hist_as0119 = dynamic_cast<TH1D*> (as0119_file->Get(hname.c_str()));
      hist_as0119->Sumw2();
    }

    //get number of bins
    Int_t nBins = hists[0]->GetNbinsX();
    //extend the range to show overflow bins
    Double_t width = hists[0]->GetBinWidth(1);

    //cout<<"nBins : "<< nBins<<endl;
    //cout<<"first bin edge: " << hists[0]->GetBinLowEdge(1)<<endl;
    //cout<<"last bin edge: " << hists[0]->GetBinLowEdge(nBins+1)+ 1.*width <<endl;

    hists[0]->GetXaxis()->SetRange(0,nBins+1);
    //SetLimits will change bin width
      //hists[0]->GetBinLowEdge(0), hists[0]->GetBinLowEdge(nBins+1) + 5.*width );

    //clone the central value hist and change bin content to max/min later
    TH1D* envHi = (TH1D*)hists[0]->Clone("High");
    TH1D* envLo = (TH1D*)hists[0]->Clone("Low");

    //find uncertainty of each bin, include underflow and overflow bins
    for(int n = 0; n<=nBins+1; n++)
    {

      //initiate a double vector bins
      vector<Double_t> bins={};

      //fill in bins vector with this bin's value of all PDF sets histogram
      for(int p=0; p<number_of_PDFsets; p++)
      {
        bins.push_back( hists[p]->GetBinContent(n) );
      }

      //initiate double, eyh-->error in y high; eyl-->error in y low
      Double_t eyh;
      Double_t eyl;

      //fill error for: MC replica or Hessian error sets
      if (isMCreplica)
      {
        //get uncertainty
        Double_t uncertainty = MCreplica_uncertainty(bins);
        //symmetric error
        eyh = uncertainty;
        eyl = uncertainty;
      }

      //Hessian sets
      else
      {
        //get uncertainties: 0. symmetric error; 1. asym error >; 2. asym error <
        vector<Double_t> uncertainties = Hessian_uncertainty(bins,scale90to68);

        //assym uncertainty
        if (asym_error)
        {
          eyh = uncertainties[1];
          eyl = uncertainties[2];
        }

        //symmetric uncertainty
        else
        {
          eyh = uncertainties[0];
          eyl = uncertainties[0];
        }
      }


      //////add alpha_s error//////////
      if (add_alpha_s_error)
      {
        //initiate a double vector bins for alpha_s, with a trivial first number
        //     (because this will be feed into Hessian_uncertainty(), first element is assumed to be the central value)
        vector<Double_t> as_bins={0.0};

        //append bin contents from alpha_s histograms
        as_bins.push_back(hist_as0117->GetBinContent(n));
        as_bins.push_back(hist_as0119->GetBinContent(n));
        vector<Double_t> as_uncertainties = Hessian_uncertainty(as_bins,false); //do not scale from 90% to 68%

        ///add error quadratically///
        double total_uncertainty_h = sqrt(eyh*eyh + as_uncertainties[0]*as_uncertainties[0]); //[0] is the symmetric error
        double total_uncertainty_l = sqrt(eyl*eyl + as_uncertainties[0]*as_uncertainties[0]);

        //update error in y high and low
        eyh = total_uncertainty_h;
        eyl = total_uncertainty_l;
      }

      ////////////////now have the final eyh and eyl////////////////////

      //get the upper envelop and lower envelop values
      double y = hists[0]->GetBinContent(n);
      envHi->SetBinContent(n, y+eyh);
      envLo->SetBinContent(n, y-eyl);
    }

    //!!!!!!!start making plots!!!!!

    // Upper plot will be in pad1, plot the histograms
    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.4, 1, 1);
    pad1->SetBottomMargin(0.05); // Upper and lower plot are joined
    //pad1->SetGridx();         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();               // pad1 becomes the current pad

    hists[0]->SetLineWidth(1);
    hists[0]->SetStats(0);          // No statistics on upper plot
    hists[0]->SetLineColor(kCyan+4); //dark green

    //copy histogram title, edit upon it
    string title = hists[0]->GetTitle();
    if (title.find("NLO")) title+= " NLO";
    hists[0]->SetTitle(title.c_str());

    envHi->SetFillColor(kSpring+5);
    envHi->SetLineWidth(0);
    envHi->SetStats(0);
    envHi->SetFillStyle(3144);
    envHi->Draw("HIST");
    envLo->SetFillColor(10); //10 while
    envLo->SetLineWidth(0);
    envLo->SetStats(0);
    envLo->Draw("HIST SAME");
    hists[0]->Draw("HIST SAME");

    ///add over/underflow
    //TH1D *hist0 = DrawOverflow(hists[0]);
    //hist0->Draw("HIST SAME");


    ////////add Legends////////////
    auto legend = new TLegend(0.6,0.65,0.95,0.92);//0.1,0.7,0.48,0.9
    legend->SetHeader(output_name.c_str(),"C"); // option "C" allows to center the header
    legend->SetTextSize(0.035);
    legend->AddEntry(hists[0],"central value","l");

    string uncertainty_legend;
    if (asym_error) uncertainty_legend = "asymmetric uncertainty";
    if (!asym_error) uncertainty_legend = "symmetric uncertainty";

    legend->AddEntry(envHi,uncertainty_legend.c_str(),"f"); //"f": fill
    legend->Draw();


    //////////add total xsection info in Latex///////////
    //get the inclusive xsection of the central value, upper env, and lower env
    double xsection0 = inclusive_xsection(hists[0])[0];
    double inclusive_xsection0 = inclusive_xsection(hists[0])[1];
    double inclusive_xsection_h= inclusive_xsection(envHi)[1];
    double inclusive_xsection_l= inclusive_xsection(envLo)[1];

    //get the difference between envelop and the central value
    inclusive_xsection_h -=inclusive_xsection0;
    inclusive_xsection_l = inclusive_xsection0 - inclusive_xsection_l;

    //make text
    string theResult = "xsection: " + to_string(xsection0);
    string theResult2 = "\n--->inclusive xsection: "+ to_string(inclusive_xsection0);
    theResult2 += "+" + to_string(inclusive_xsection_h) + "/-" + to_string(inclusive_xsection_l)+ " fb";

    TLatex *theText = new TLatex(0.54,0.57, theResult.c_str());
    theText->SetNDC();
    theText->SetTextSize(.043);
    theText->Draw();

    TLatex *theText2 = new TLatex(0.54,0.52, theResult2.c_str());
    theText2->SetNDC();
    theText2->SetTextSize(.043);
    theText2->Draw();

    string NumEntries = "Total Entries: "+ to_string(hists[0]->GetEntries());

    TLatex *theText3 = new TLatex(0.54,0.47, NumEntries.c_str());
    theText3->SetNDC();
    theText3->SetTextSize(.043);
    theText3->Draw();

    //////////////////finished with Pad1/////////////////////////////



    /// /lower plot will be in pad2, ratio plot//////////////
    can->cd();          // Go back to the main canvas before defining pad2
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0.02, 1, 0.4);
    pad2->SetTopMargin(0.02);
    pad2->SetBottomMargin(0.1);
    //pad2->SetGridx(); // vertical grid
    pad2->Draw();
    pad2->cd();       // pad2 becomes the current pad


    //let the first hist to be the baseline
    TH1D *ratioHi = (TH1D*)envHi->Clone("ratio high");
    ratioHi->SetStats(0);      // No statistics on lower plot
    ratioHi->Divide(hists[0]); //default the first hist is baseline
    ratioHi->SetFillColor(kSpring+5);
    ratioHi->SetLineWidth(0);
    ratioHi->SetFillStyle(3144);
    ratioHi->Draw("HIST");
    ratioHi->SetTitle(""); // Remove the ratio title

    TH1D *ratioLo = (TH1D*)envLo->Clone("ratio low");
    ratioLo->Divide(hists[0]); //default the first hist is baseline
    ratioLo->SetFillColor(10); //10 while
    ratioLo->SetLineWidth(0);
    ratioLo->SetStats(0);
    ratioLo->Draw("HIST SAME");

    ratioHi->SetMinimum(0.9);  // Define Y ..
    ratioHi->SetMaximum(1.1); // .. range
    string ratioTitle = "ratio baseline: central value";
    ratioHi->GetYaxis()->SetTitle(ratioTitle.c_str());
    ratioHi->GetYaxis()->SetNdivisions(505);
    ratioHi->GetYaxis()->SetTitleSize(14);
    ratioHi->GetYaxis()->SetTitleFont(43);
    ratioHi->GetYaxis()->SetTitleOffset(1);
    ratioHi->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
    ratioHi->GetYaxis()->SetLabelSize(9);

    ratioHi->GetXaxis()->SetTitleSize(20);
    ratioHi->GetXaxis()->SetTitleFont(43);
    ratioHi->GetXaxis()->SetTitleOffset(1.);
    ratioHi->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
    ratioHi->GetXaxis()->SetLabelSize(12);

    //draw a line at y=1,
    //!!!draw last!!! otherwise will cause problem
    Double_t BinW = hists[0]->GetBinWidth(0);
    Double_t x_max = Double_t(nBins)*BinW;
    TLine *line = new TLine(0, 1, x_max, 1);
    line->SetLineColor(kCyan+4);
    line->SetLineWidth(1);
    line->Draw("SAME");

    //same to pdf
    can->Print(save.c_str(),"pdf");
    can->Clear();
    //////finished making PDF sets uncertainty plots //////////////


  }

  //close pdf
  can->Print(save_end.c_str(),"pdf");

//  vector<Double_t> bins={1.,2.,3.,4.,5.};
//  Double_t error = MCreplica_uncertainty(bins);
//  cout<<error<<endl;
}

//return the 68% CL symmetric+assym uncertainty of a PDF sets
vector<Double_t> Hessian_uncertainty(vector<Double_t> bin_contents, bool scale90to68)
{
  using namespace std;
  // the CT14 variations cover a 90% interval (1.645sigma), hence scale down variations to get a 68% one
  double CLcorrectionFactor = 1./1.645;
  if (!scale90to68) CLcorrectionFactor = 1.;
  const int num_eigenvector = (bin_contents.size()-1)/2;

  Double_t sum_sym = 0.0;
  Double_t sum_asym_bigger = 0.0;
  Double_t sum_asym_smaller = 0.0;
  Double_t X_central = bin_contents[0];
  Double_t X_plus;
  Double_t X_minus;
  for (int i = 0; i < num_eigenvector; i++)
  {
    // member set number
    int member_set_num = i*2 + 1;
    X_plus = bin_contents[member_set_num];
    X_minus= bin_contents[member_set_num+1];

    sum_sym+=(X_plus - X_minus) * (X_plus - X_minus);

    Double_t max_bigger = max(X_plus-X_central, max(X_minus-X_central, 0.0));
    sum_asym_bigger += max_bigger * max_bigger;

    Double_t max_smaller = max((X_central-X_plus), max((X_central-X_minus), 0.0));
    sum_asym_smaller += max_smaller * max_smaller;
  }

  Double_t error90 = sqrt(sum_sym)*0.5;
  //cout<<error90<<endl;
  Double_t error68_sym = error90 * CLcorrectionFactor;

  Double_t error68_asym_b = sqrt(sum_asym_bigger)*CLcorrectionFactor;
  Double_t error68_asym_s = sqrt(sum_asym_smaller)*CLcorrectionFactor;

  vector<Double_t> uncertainties = {error68_sym,error68_asym_b,error68_asym_s};
  //cout<<uncertainties[0]<<"  "<<uncertainties[1]<<"  "<<uncertainties[2]<<endl;
  return uncertainties;
}

Double_t MCreplica_uncertainty(vector<Double_t> bin_contents)
{
  using namespace std;
  Int_t num_of_error_sets = bin_contents.size();
  Double_t X_central = bin_contents[0];

  Double_t sum = 0.0;
  Double_t Xi;

  //iterate through all sets (except for the first cuz it's the central value)
  for (int i = 1; i < num_of_error_sets; i++)
  {
    // member set value
    Xi = bin_contents[i];
    sum += (Xi - X_central) * (Xi - X_central);
  }

  //make a double version of number of error sets
  Double_t N = (double)num_of_error_sets - 1.;
  //cout << N/3. << endl;

  Double_t uncertainty=0;

  //incase want to only look at the central value, N=0
  if (N>0) uncertainty = sqrt(sum/N);

  return uncertainty;

}

vector<Double_t> inclusive_xsection(TH1D* hist)
{
  using namespace std;
  Double_t inclusive = 0;
  vector<Double_t> xsection;
  Int_t nBins = hist->GetNbinsX();
  //cout << "/ Without under/over flow bins /" << '\n';
  for (Int_t n = 1; n <= nBins; n++) {
    inclusive += (hist->GetBinContent(n)) * (hist->GetBinWidth(n));
  }
  string theResult = "XSection: " + to_string(inclusive) + " fb";
//  cout << theResult << '\n';
  //inclusive = hist->Integral("width");
  xsection.push_back(inclusive);

  //cout << "/ With under/over flow bins /" << '\n';
  inclusive += (hist->GetBinContent(0)) * (hist->GetBinWidth(0));
  inclusive += (hist->GetBinContent(nBins + 1)) * (hist->GetBinWidth(nBins + 1));
  theResult = "---> Inclusive XSection: " + to_string(inclusive) + " fb";
  //cout << theResult << '\n';

  xsection.push_back(inclusive);

  return xsection;
}

TH1D * DrawOverflow(TH1D *h)
{
      // This function paint the histogram h with an extra bin for overflows
   UInt_t nx    = h->GetNbinsX()+1;
   Double_t *xbins= new Double_t[nx+1];
   for (UInt_t i=0;i<nx;i++)
     xbins[i]=h->GetBinLowEdge(i+1);
   xbins[nx]=xbins[nx-1]+h->GetBinWidth(nx);
   char *tempName= new char[strlen(h->GetName())+10];
   sprintf(tempName,"%swtOverFlow",h->GetName());
   // Book a temporary histogram having ab extra bin for overflows
   TH1D *htmp = new TH1D(tempName, h->GetTitle(), nx, xbins);
   // Reset the axis labels
   htmp->SetXTitle(h->GetXaxis()->GetTitle());
   htmp->SetYTitle(h->GetYaxis()->GetTitle());
   // Fill the new hitogram including the extra bin for overflows
   for (UInt_t i=1; i<=nx; i++)
     htmp->Fill(htmp->GetBinCenter(i), h->GetBinContent(i));
   // Fill the underflows
   htmp->Fill(h->GetBinLowEdge(1)-1, h->GetBinContent(0));
   // Restore the number of entries
   htmp->SetEntries(h->GetEntries());
   // FillStyle and color
   htmp->SetFillStyle(h->GetFillStyle());
   htmp->SetFillColor(h->GetFillColor());


   Double_t width = h->GetBinWidth(1);
   htmp->GetXaxis()->SetRange( h->GetBinLowEdge(0) - width , h->GetBinLowEdge(nx) + 2.*width );

   return htmp;
}
