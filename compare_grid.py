import os, csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def getGrid(path):
    grid1 = []
    with open(path, 'r') as file:
        for row in file:
            line = row.split()
            for i, num in enumerate(line):
                num1 = float( '%.4g' %float(num[:-4]))
                expo1 =  float(num[-3:])
                line[i] = num1*(10.**expo1)
            grid1.append(line)

    return grid1

def rms(array):
    blah=np.sum([a*a for a in array])/float(len(array))
    return np.sqrt(blah)

def SavePlot(path, fname):
    if not os.path.isdir(path):
        os.makedirs(path)
    plt.savefig(path + fname + '.pdf')
    plt.close()

def main():
    #dir = "../LO_it6_pts22_2/"
    #dir = "../LO_it8_pts24_0/"
    single_run = False

    if (single_run):
        dir = "../grid5-26_it5_pts31_0/"
        order = '3'

        grid_file = "grid"+order+"_1.out."
        iteration = 0
        for file in os.listdir(dir):
            if grid_file in os.path.basename(file):
                iteration+=1

        print("there are ", iteration, "grids")

        #grid_path1 = grid_file+"1"
        #grid1 = getGrid(grid_path1)
        grids=[]
        for i in range(iteration):
            n=str(i+1)
            grid_path = grid_file+n
            grid = getGrid(dir+grid_path)
            grids.append(grid)

    else:
        dir = "../tight_cut_dum_NLO_it1_pts31/"
        #grids_path=["../loose_cuts_it5_pts26_0/grid3_1.out.5","../loose_cuts_grid_it1_pts26_0/grid3_1.out.1","../loose_cuts_grid_it1_pts26_0/grid3_1.out.1","../loose_cuts_grid_it1_pts26_0/grid3_1.out.1"]
        #grids_path = ["../new_cuts_pts23_0/grid3_1.out.1","../new_cuts_pts23_0/grid3_1.out.2",
                      #"../new_cuts_pts25_0/grid3_1.out.1","../new_cuts_pts25_0/grid3_1.out.2",
                     # "../new_cuts_pts27_1/grid3_1.out.1","../new_cuts_pts27_1/grid3_1.out.2",
                     # "../new_cuts_pts29_2/grid3_1.out.1","../new_cuts_pts29_2/grid3_1.out.2",
                     # "../new_cuts_pts31_3/grid3_1.out.1","../new_cuts_pts31_3/grid3_1.out.2"]
        #"../tight_cut_dum_it2_pts21_0/grid2_1.out.1","../tight_cut_dum_it2_pts21_0/grid2_1.out.2"]
        grids_path=["../new_cuts_pts31_3/grid3_1.out.2","../tight_cut_dum_NLO_it1_pts31/grid3_1.out.1"]
        iteration = len(grids_path)
        grids=[]
        for i in range(iteration):
            grid = getGrid(grids_path[i])
            grids.append(grid)

    #get histogram
    STD =  []
    names = []
    for i in range(iteration-1):
        grid1 = np.asarray(grids[i]).flatten()
        grid2 = np.asarray(grids[i+1]).flatten()
        grid_diff = np.subtract(grid1,grid2)

        grid_diff_std = rms(grid_diff) #np.std(grid_diff)
        print(i,i+1)
        print(grid_diff_std)
        STD.append(grid_diff_std)

        fig = plt.figure("Grid Difference: iteration" + str(i)+' - '+ str(i+1) , (10,10))
        ax = fig.add_subplot(111)

        #bins = np.arange(-30, 35, 7.5)
        plt.hist(grid_diff)#, bins=bins)
        plt.xlabel('$\Delta$' + "grid" , fontsize=18)
        plt.ylabel('Counts ', fontsize=18)
        name = str(i+1)+'-'+ str(i+2)
        names.append(name)
        SavePlot(dir, name)

    #fig = plt.figure("standard deviation evolution over iteration" , (10,10))
    #ax = fig.add_subplot(111)

    x = np.arange(0, len(names), 1)
    plt.xticks(x,names)
    plt.plot(x,STD)#, bins=bins)
    plt.xlabel("iterations" , fontsize=18)
    plt.ylabel('RMS', fontsize=18)
    plt.ylim(0,0.04)
    SavePlot(dir, "std evolution")


if __name__ =='__main__':
    main()
