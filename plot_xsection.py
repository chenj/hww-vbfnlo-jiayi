#!/usr/bin/env python

import os
import pandas as pd
import matplotlib.pyplot as plt
from pylab import rcParams
import re
from compare_grid import SavePlot

#paths = ["../LO_it1_pts17", "../LO_it2_pts18","../LO_it6_pts22","../LO_it8_pts24"]
#paths = ["../LO_it4_pts20","../LO_grid_it1_pts19", "../LO_grid_it1_pts18","../LO_grid_it1_pts17"]
#paths = ["../LO_it4_pts20","../LO_grid_it1_pts19", "../LO_grid_it1_pts18","../LO_grid_it1_pts17"]
#paths=["../loose_cuts_it5_pts26_0","../loose_cuts_it5_pts31_0"]
#os.chdir(path)
paths=["../new_cuts_pts23_0","../new_cuts_pts25_0","../new_cuts_pts27_1","../new_cuts_pts29_2","../new_cuts_pts31_3"]
results=[]
folders=[]
index=0
for folder in paths: #os.listdir(path):

    if os.path.isdir(folder):
        folder_name = os.path.basename(folder)
        print(folder_name)
        folders.append(folder_name)

        xsec_file = folder+'/'+"xsection.out"
        result = pd.read_csv(xsec_file, header=None, delim_whitespace=True)
        dict={"LO": result[0][0],"LO error": result[1][0], "NLO":result[0][1],"NLO error":result[1][1]}
        reformat = pd.DataFrame(data=dict,index=[index])

        #reformat.insert(0,"parameter",)
        #print(result)
        #reformat.set_index("parameter",inplace=True)
        results.append(reformat)
        #xsection[folder_name] = result

xsection=pd.concat(results)
print(folders)
folders = ["pts23","pts25", "pts27", "pts29","pts31"]
xsection.insert(0,"parameters",folders)
print(xsection)

#ax = plt.gca()
#plt.figure(figsize=(8, 6), facecolor='w', edgecolor='k')
#rcParams['figure.figsize'] = 5, 10

fig, axes = plt.subplots(nrows=2,ncols=1,sharex=True)

xsection.plot(kind='line',x='parameters',y='NLO',color='red',ax=axes[0])

xsection.plot(kind='line',x='parameters',y="NLO error",color='blue',ax=axes[1])

#plt.xlim(-1,len(folders))
#axes[1].set_xticklabels(folders)
#axes[1].tick_params(labelrotation=30)
#axes[1].set_xlim(-0.05,len(folders)-0.9)
axes[0].set_ylabel("xsection (pb)")
axes[1].set_ylabel("xsection error (pb)")
axes[0].set_ylim(13.5,14.5)
axes[1].set_ylim(-0.1,1)
plt.tight_layout()
SavePlot("./compare_xsections/", "new cuts NLO xsection ")
#plt.show()

#names=["xsection","error"]
#xsection.columns=names
#print(xsection)
#print(xsection.keys())
#print(xsection["LO"])
#print(xsection.loc[0].keys())
#print(result["error"]["NLO"])
#print(result.loc["LO",:])
