#!/usr/bin/env python

#Created      Sep30, 2019 --Jiayi Chen, jennyz@brandeis.edu
#last update: Dec9, 2019 --Jiayi

import io, argparse, json, ast, os, time

#def findNoneZeroParam(infile):

class Basis_Parameters(object):

    def __init__(self, input_path = "./", BasisCard = "HISZ_myinput.dat"):
        if input_path[-1] != '/':
            input_path+="/"
        self.inputCard = input_path + BasisCard
        self.param_dict = {}
        self.getBasis()


    def getBasis(self):

        with open(self.inputCard, 'r') as file:
            # read a list of lines into data
            lines = file.readlines()

        for i, line in enumerate(lines):
            if "INFORMATION FOR" in line:
                start_ind = line.index("INFORMATION FOR")+len("INFORMATION FOR")
                self.basis = line[start_ind:].strip('\n').strip('BASIS').strip()
                print("the basis is ", self.basis)

                basis_info_lineno=i+2
                self.lines = lines[basis_info_lineno:]
                break
        #self.lines.remove('\n')
        #print(self.lines)
        #for i, line in enumerate(self.lines):

        #for line in lines[basis_info_lineno+1:]:

    def getParameters(self):

        for ind, line in enumerate(self.lines):
            #skip empty line
            if line == '\n':
                continue
            split_line = line.split()

            #skip
            if len(split_line) < 2:
                continue

            #skip line that the second last element is not #
            if "#" not in split_line[-2]:
                continue
            param_name = split_line[-1]
            param_value = split_line[-3]
            self.param_dict[param_name] = float(param_value)


if __name__ == '__main__':
    basis = Basis_Parameters()
    basis.getParameters()
    print(basis.param_dict)
