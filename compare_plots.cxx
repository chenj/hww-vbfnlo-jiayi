#include "TFile.h"
#include "TTree.h"
#include "TLatex.h"
#include "TLegend.h"
#include <iostream>

Double_t negative_xsec(TH1F);

void compare_plots(string changed_param,bool readRootFiles = false, bool normalize = false,
                   bool add_NLO = true, bool stack_LO_NLO = false, bool envelop = true)
/*
changed_para:    the name of the generated PDF, should indicate the paramter that was varied in this compare compare_plots
readRootFiles:   at the beginning, histograms.root are stored in parallel at the same directory level (true); in the case
                 where .root files are saved in different directories (false)
normalize:       normalize differential distribution (true); do not normalize (false)
add_NLO:         also compare NLO distribution (true); only compare LO (false)
stack_LO_NLO:    compare LO with NLO (true) --> can only have one single histograms.root in this case
                                            --> add_NLO boolean will be ignored
envelop:         in the ratio plot, want envelop (true) --> the individual histogram's ratio plot will not show
                 no envolp (false)                      --> individual histogram ratio will be plotted
*/
{
  using namespace std;
  //xsection.out = 5.9801263306982539 fb
//////////////////////////////////////////////////////////////
//                                                          //
//               prepare file name and description          //
//                                                          //
//////////////////////////////////////////////////////////////
 string folder_prefix = "../CT14nlo-0";
 vector<string> fnames = {"muR1.0muF1.0","muR1.0muF0.5","muR1.0muF2.0","muR0.5muF1.0","muR2.0muF1.0","muR2.0muF2.0","muR0.5muF0.5","muR2.0muF0.5","muR0.5muF2.0"};
   //"CT14nlo/CT14nlo-PDF-0/","MMHT14/MMHT14-0","NNPDF30/NNPDF30-as0118-0"};
   //"RJJ_MINdefaultCuts-muR2-0.4D0","RJJ_MINdefaultCuts-muR2-1.0D0","RJJ_MINdefaultCuts-muR2-2.0D0",
  //"RJJ_MINdefaultCuts-muR2-3.0D0","RJJ_MINdefaultCuts-muR2-4.0D0"};
   //"CT14nlo-PDF-CT14nlo_as_0118-muR-muF","CT14nlo-PDF-CT14nlo_as_0117-muR-muF","CT14nlo-PDF-CT14nlo_as_0119-muR-muF"};
//"RJJ_MINdefaultCuts_-0.1","RJJ_MINdefaultCuts_-0.4","RJJ_MINdefaultCuts_-1","RJJ_MINdefaultCuts_-2"};
                          //"new_cuts_pts31_3","tight_cut_RJJ_it1_pts29"};
                          //"tight_cut_dum2_NLO_it1_pts31"};
                          //"tight_cut_dum2_NLO_it1_pts30","tight_cut_dum2_NLO_it1_pts29"};
                          //"new_cuts_pts31_3","new_cuts_pts29_2","new_cuts_pts27_1"};
                          //"tight_cut_it4_pts20_0","tight_cut_it1_pts20_2"};//"tight_cut_it2_pts21_0","tight_cut_it1_pts21_1",
                          //"loose_cuts_it5_pts31_0","grid5-26_it5_pts31_0","loose_cuts_it5_pts26_0"};
                          //"wwjj_cW0cWWW0cB0_truth_cuts","new_loose_cuts_RJL_MIN_0","new_loose_cuts_RLL_MIN_1","new_loose_cuts_RJJ_Min_2"};
                          //"loose_cuts_it5_pts31_0","grid5-31_it1_pts28_0","grid5-31_it1_pts27_0"};
                          //"loose_cuts_it5_pts26_0","loose_cuts_grid_it1_pts26_0","loose_cuts_grid_it1_pts26_1","loose_cuts_grid_it1_pts26_2"};
                          //"wwjj_cW0cWWW0cB0_truth_cuts", "loose_cuts2_ETAJJ_MIN_0", "loose_cuts2_RJL_MIN_1",
                          //"loose_cuts2_RLL_MIN_2", "loose_cuts2_PT_L_MIN_3", "loose_cuts2_PT_JET_MIN_5",
                          //"loose_cuts2_RJJ_Min_6", "loose_cuts2_DELY_JL_7"};
                          //"wwjj_cW0cWWW0cB0_truth_cuts","loose_cuts1_RJJ_Min_0","loose_cuts1_PT_JET_MIN","loose_cuts1_Y_L_MAX","loose_cuts1_RJL_MIN","loose_cuts1_RLL_MIN",
                          //"loose_cuts1_DELY_JL","loose_cuts1_PT_L_MIN","loose_cuts1_ETAJJ_MIN"};

                          //{"wwjj_cW0cWWW0cB0_truth_cuts","cuts_remove0_RJJ_MIN","cuts_remove1_PTMIN_TAG_2",
                          //"cuts_remove2_PTMIN_TAG_1","cuts_remove3_DELY_JL","cuts_remove4_PT_L_MIN"};
                          //{"cuts_remove9_Y_L_MAX","cuts_remove0_MLL_MIN","cuts_remove1_JVETO","cuts_remove2_PTMISS_MIN","cuts_remove3_LRAPIDGAP"};
                          //"cW0cWWW0cB0_muF1muR1", "cW0cWWW0cB0_muF1muR1_truth_cuts"};
                          //compare fwww// "cW0cWWW1cB0_muF1muR1","cW0cWWW2cB0_muF1muR1","cW0cWWW5cB0_muF1muR1","cW0cWWW10cB0_muF1muR1","cW0cWWW20cB0_muF1muR1"};


 //initiate a list of TFiles and a list of parameter description
 TFile *files[fnames.size()];
 string param_description[fnames.size()];
 int i = 0;

   //fill in files to the list and get the parameter description
   for(auto fname : fnames)
   {
      if (readRootFiles){
       files[i] = new TFile(fname.c_str());
       string word = "grams_";
       string root = ".root";
       size_t ind_i = fname.find("grams"); //grams in histograms

       //get the description: histograms_description.root
       param_description[i] = fname.substr(ind_i + word.length(), fname.length() - root.length() - (ind_i + word.length()) );
      }

      //if read from a folder with all vbfnlo output
      else{

        param_description[i] = fname;//fname is the folder name and is the parameter description
        string new_fname = folder_prefix + fname + "/histograms.root";//get histograms.root in the folder
        fnames[i] = new_fname;
        files[i] = new TFile(new_fname.c_str());
        //cout<<"in here!!"<<endl;
      }
     cout<<"loading root file: "<<fnames[i]<<endl;
     cout << "Comparing" << param_description[i] << '\n';
     i++;
   }


 //////////////////////////////////////////////////////////////
 //                                                          //
 //               prepare histogram to compare               //
 //                                                          //
 //////////////////////////////////////////////////////////////

  // histogram names
  vector<string> hnames = { //jets -- energy
                           //"H_ptj_LO", //
                           "H_pTjj",//, // di-jet system pT
                           "H_mjj",
                           "H_HTjj",
                           "H_pTj1",
                           "H_pTj2",
                           //"H_pTj_min",
                           //jets -- angles
                           "H_phijj",
                           "H_etajj",
                           //number of jets
                           "H_Number_of_j",
                           //leptons -- energy
                           "H_pTl1",
                           "H_pTl2",
                           "H_mll",
                           "H_pTllnnu",
                           //"H_pllnnu",
                           //leptons -- angles
                           "H_phill"
                           };

  //get the correct LO and NLO names///
  vector<string> gethnames;
  int num_hnames = hnames.size();
  for (int h=0; h<num_hnames; h++){
    string hname_LO = hnames[h]+"_LO";
    gethnames.push_back(hname_LO);

    //if request to add NLO plots and do not want to overlap NLO&LO
    if(add_NLO & !stack_LO_NLO){
      string hname_NLO = hnames[h]+"_NLO";
      gethnames.push_back(hname_NLO);
    }
  }

  hnames = gethnames;

/////////////////////////finished initializing TFiles and hist names////////////////////////////////////////

  gROOT->SetBatch(kTRUE);
  TCanvas* can = new TCanvas();

  //open a pdf file and print the canvas in the pdf
  string save = changed_param + ".pdf" ; //hname + "_" +
  string save_begin = save+"(";
  string save_end = save+")";
  can->Print(save_begin.c_str(),"pdf");

  //iterate through each type of the histograms
  for (auto hname : hnames) {

    cout << "/ ========== /" << '\n';
    cout << hname << '\n';

    // Upper plot will be in pad1, plot the histograms
    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.4, 1, 1);
    pad1->SetBottomMargin(0.05); // Upper and lower plot are joined
    //pad1->SetGridx();         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();               // pad1 becomes the current pad

    //initiate stack hist
    auto hs = new THStack("hs",hname.c_str());

    //initiate legend
    auto legend = new TLegend(0.6,0.55,0.95,0.92);//0.1,0.7,0.48,0.9
    legend->SetHeader(changed_param.c_str(),"C"); // option "C" allows to center the header
    legend->SetTextSize(0.04);

    //initiate a list of TH1Fs
    TH1F** hists = new TH1F*[fnames.size()];

    //initiate a list of TH1Fs for NLO
    TH1F** hists_NLO = new TH1F*[fnames.size()];

    //iterate through all root files
    for (int n=0; n<fnames.size(); n++){

      //copy over the hist from root file
      hists[n] = dynamic_cast<TH1F*> (files[n]->Get(hname.c_str()));
      hists[n]->Sumw2();

      //if request to normalize the distribution
      Double_t norm = 1;
      if (normalize){
        //scale factor = 1/total xsection
        Double_t scale = norm/(hists[n]->Integral());
        hists[n]->Scale(scale);
      }

      //////check negative xsec/////
      Double_t neg_xsec = negative_xsec(*hists[n]);
      if (neg_xsec<0){
        cout<<"NEGATIVE XSEC WARNING:" << fnames[n] << "   xsec = " << neg_xsec << endl ;
      }

      hists[n]->SetLineWidth(1);
      hists[n]->SetStats(0);          // No statistics on upper plot
      hists[n]->SetLineColor(n+1);

      //add a hist of each root file to the THStack
      hs->Add(hists[n]);

      //add legend
      legend->AddEntry(hists[n],param_description[n].c_str(),"l");

      //////stack LO with NLO////////////
      if (stack_LO_NLO){
        string LO = "LO";
        string NLO = "NLO";
        size_t ind = hname.find(LO); //grams in histograms

        //get the NLO in hname
        string hname_NLO = hname.substr(0, ind);
        hname_NLO += NLO;
        //cout << "copying the NLO hist " << hname_NLO << endl;

        hists_NLO[n] = dynamic_cast<TH1F*> (files[n]->Get(hname_NLO.c_str()));
        //TH1F* hist_NLO = dynamic_cast<TH1F*> (files[n]->Get(hname_NLO.c_str()));
        hists_NLO[n]->Sumw2();

        if (normalize){
          Double_t scale = norm/(hists_NLO[n]->Integral());
          hists_NLO[n]->Scale(scale);
        }

        hists_NLO[n]->SetLineWidth(1);
        hists_NLO[n]->SetStats(0);          // No statistics on upper plot
        hists_NLO[n]->SetLineColor(n+1+fnames.size());
        hists_NLO[n]->SetLineStyle(7);
        hs->Add(hists_NLO[n]);

        //add in legend "NLO"
        string description = param_description[n] + "_NLO";
        legend->AddEntry(hists_NLO[n], description.c_str() ,"l");
      }
/////stack LO NLO end////////////////////

    }

    //edit original title indicate, NLO/LO and normalize/not normalized
      string title = hists[0]->GetTitle();
      if (!stack_LO_NLO) title =title + " - " + hname;
      if (normalize) title += " (normalized)";
      if (!normalize) title += "(NOT normalized)";
      hs->SetTitle(title.c_str());
      hs->Draw("nostack HIST");
      legend->Draw();
/////////////////////finished Pad1//////////////////////


/////////  // lower plot will be in pad2, ratio plot/////////////////////////
    can->cd();          // Go back to the main canvas before defining pad2
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0.02, 1, 0.4);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.1);
    //pad2->SetGridx(); // vertical grid
    pad2->Draw();
    pad2->cd();       // pad2 becomes the current pad

    //initiate a list of ratio hist
    TH1F** ratio = new TH1F*[fnames.size()];
    TH1F** ratio_NLO = new TH1F*[fnames.size()];

    //iterate through the rest of the hist
    for (int n=0; n<fnames.size(); n++){

      //clone each hist and divide by the baseline hist
      ratio[n] = (TH1F*)hists[n]->Clone("ratioPlot");
      ratio[n]->SetStats(0);      // No statistics on lower plot
      ratio[n]->Divide(hists[0]); //default the first hist is baseline

      //plot individual ratio if requires no envelop///
      if(!envelop)
      {
        ratio[n]->Draw("hist p same");
        ratio[n]->SetMarkerStyle(4);
        ratio[n]->SetMarkerSize(0.6);
        ratio[n]->SetMarkerColor(n+1);
        ratio[n]->SetTitle(""); // Remove the ratio title
      }

      //if ask to stack LO and NLO, add NLO to ratio
      if (stack_LO_NLO){
        ratio_NLO[n] = (TH1F*)hists_NLO[n]->Clone("ratioNLOPlot");
        ratio_NLO[n]->SetStats(0);      // No statistics on lower plot
        ratio_NLO[n]->Divide(hists[0]); //default the first hist is baseline
        //plot individual ratio if requires no envelop///
        if(!envelop)
        {
          ratio_NLO[n]->Draw("hist p same");
          ratio_NLO[n]->SetMarkerStyle(24);
          ratio_NLO[n]->SetMarkerSize(0.4);
          ratio_NLO[n]->SetMarkerColor(n+fnames.size()+1);
          ratio_NLO[n]->SetTitle(""); // Remove the ratio title
        }

      }

    }

    ///////ratio upper and lower bounds//////////
      ratio[0]->SetMinimum(0.5);  // Define Y ..
      ratio[0]->SetMaximum(1.5); // .. range

    ////plot y-axis title indicate baseline///
      string ratioTitle = "baseline: "+ param_description[0];
      ratio[0]->GetYaxis()->SetTitle(ratioTitle.c_str());
      ratio[0]->GetYaxis()->SetNdivisions(505);
      ratio[0]->GetYaxis()->SetTitleSize(14);
      ratio[0]->GetYaxis()->SetTitleFont(43);
      ratio[0]->GetYaxis()->SetTitleOffset(1);
      ratio[0]->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
      ratio[0]->GetYaxis()->SetLabelSize(10);

      // X axis ratio plot settings
      //ratio->GetXaxis()->SetLimits(0.,x_max);                 // along X
      ratio[0]->GetXaxis()->SetTitleSize(20);
      ratio[0]->GetXaxis()->SetTitleFont(43);
      ratio[0]->GetXaxis()->SetTitleOffset(1.);
      ratio[0]->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
      ratio[0]->GetXaxis()->SetLabelSize(15);

////////make envelop////////
   Int_t nBins = hists[0]->GetNbinsX();
   Double_t BinW = hists[0]->GetBinWidth(0);
///initiate ratio plot with the baseline ratio hist (y=1)////////
   TH1F *ratioHi = (TH1F*)ratio[0]->Clone("ratio high");
   TH1F *ratioLo = (TH1F*)ratio[0]->Clone("ratio low");

   /////find max and min of each bin/////
   for (Int_t n = 1; n <= nBins; n++) {

     //initiate with the baseline bin content///
     Double_t max = ratio[0]->GetBinContent(n);
     Double_t min = ratio[0]->GetBinContent(n);

     //iterate through the rest of the hists///
     for(Int_t h = 1; h < fnames.size(); h++){

       Double_t BinContent = ratio[h]->GetBinContent(n);
       if (BinContent > max) max = BinContent;
       if (BinContent < min) min = BinContent;
     }

     //set values to max/min ////
     ratioHi->SetBinContent(n,max);
     ratioLo->SetBinContent(n,min);

   }
   ratioHi->SetStats(0);      // No statistics on lower plot
   ratioHi->SetFillColor(kSpring+5);
   ratioHi->SetLineWidth(0);
   ratioHi->SetFillStyle(3144);
   ratioHi->SetTitle(""); // Remove the ratio title

   ratioLo->SetStats(0);      // No statistics on lower plot
   ratioLo->SetFillColor(10);
   ratioLo->SetLineWidth(0);
   ratioLo->SetTitle(""); // Remove the ratio title

////plot envelop//////
  if (envelop){
    ratioHi->Draw("HIST SAME");
    ratioLo->Draw("HIST SAME");

    //draw a line at y=1
    Double_t x_max = Double_t(nBins)*BinW;
    TLine *line = new TLine(0, 1, x_max, 1);
    line->SetLineColor(1);
    line->SetLineWidth(2);
    line->Draw("SAME");

    //ratio[0]->Draw("hist p same");
    //ratio[0]->SetMarkerStyle(4);
    //ratio[0]->SetMarkerSize(0.6);
    //ratio[0]->SetMarkerColor(8);
    //ratio[0]->SetTitle(""); // Remove the ratio title
    ratio[7]->Draw("hist p same");
    ratio[7]->SetMarkerStyle(4);
    ratio[7]->SetMarkerSize(0.6);
    ratio[7]->SetMarkerColor(8);
    ratio[7]->SetTitle(""); // Remove the ratio title

    ratio[8]->Draw("hist p same");
    ratio[8]->SetMarkerStyle(4);
    ratio[8]->SetMarkerSize(0.6);
    ratio[8]->SetMarkerColor(9);
    ratio[8]->SetTitle(""); // Remove the ratio title

   }

   can->Print(save.c_str(),"pdf");
   can->Clear();

  }
  //close pdf
  can->Print(save_end.c_str(),"pdf");
}


Double_t negative_xsec(TH1F hist)
{
  Double_t neg_xsec = 0.0;
  Int_t nBins = hist.GetNbinsX();
  for (Int_t n = 1; n <= nBins; n++) {
    Double_t binCont = hist.GetBinContent(n);

    if (binCont<0){
      neg_xsec += binCont* (hist.GetBinWidth(n));
    }
  }
  return neg_xsec;
}
