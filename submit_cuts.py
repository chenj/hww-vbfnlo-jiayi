#!/usr/bin/env python

#Created      Aug28, 2019 --Jiayi Chen, jennyz@brandeis.edu
#last update: Aug29, 2019 --Jiayi

import io, argparse, json, ast, os, time
#from change_input_VBFNLO import changeParameter

#change parameter in the given file_name from the input path and dump it to the output path
def changeParameter(input_path = './', output_path = './',
                file_name = 'cuts.dat', name = '', value = ''):

    #input_path = args.input_path
    print(input_path)
    if input_path[-1] != '/':
        input_path+="/"
    input_path+=file_name

    output_path =output_path + file_name

    print("openning input file: ", input_path)

    with open(input_path, 'r') as file:
        # read a list of lines into data
        lines = file.readlines()

    #a flag to confirm the parameter name is correct
    name_is_found = False

    #look for the given parameter
    for ind, line in enumerate(lines):
        if name in line:

            lineno = ind
            name_is_found = True
            start_ind = line.index('=')

            end_ind = -1
            #keep the comments starting with !
            if "!" in line:
                end_ind = line.index('!')

            #create new line, inserting new value
            new_line = line[:start_ind] + "=" + value + line[end_ind:]
            break

    if name_is_found:
        print("new parameter:", new_line)

        #replace parameter value
        lines[lineno] = new_line
        with open(output_path, 'w') as file:
            file.writelines( lines )
        print("new file saved to ", output_path)

    else:
        print("did not find the given parameter name: ", name)

def main(action, param_path = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/myinput/VBFNLO/"):

    VBFNLO_Analysis_path = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/VBFNLO_Analysis/"
    parameter_lists = VBFNLO_Analysis_path+"new_parameters.json"

    #action = "remove" #for naming the directory

    #origin of the default .dat files
    default_param_path = param_path
    #default_param_path = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/myinput/VBFNLO/"

    #read json
    with open(parameter_lists, 'r') as file:
        #read a list of lines into data
        list_of_jobs = json.load(file)

    #translate json to dict
    list_of_jobs = ast.literal_eval(json.dumps(list_of_jobs))

    #initiate previous_directory to copy .dat files from and edit on top of
    previous_dir = default_param_path

    #iterate throu all jobs
    for ind, parameters in enumerate(list_of_jobs):
        #iterate throu dict: 1. .dat file name
        for file_name in parameters.keys():
            print("=====change parameters in ", file_name, "=====")

            #iterate throu dict: 2. parameter need to be changed in this .dat file
            for name in parameters[file_name].keys():

                #change directory into the VBFNLO Analysis directory
                os.chdir(VBFNLO_Analysis_path)

                #get the new value for this parameter
                value = parameters[file_name][name]

                new_dir = VBFNLO_Analysis_path + file_name.strip(".dat") + "_" + action +str(ind)+ "_" + name +'/'
                new_dir = new_dir.strip()
                os.mkdir(new_dir)

                print("created new directory", new_dir)

                #go to the new directory, need to run VBFNLO here
                os.chdir(new_dir)

                #path to copy .dat files from
                vbfnlo_in_path  = previous_dir+"vbfnlo.dat"  #
                anomV_in_path   = previous_dir+"anomV.dat"   # change to
                anomHVV_in_path = previous_dir+"anom_HVV.dat"# a for loop maybe?
                cuts_in_path    = previous_dir+"cuts.dat"    #

                #copy files from the previous directory to new directory
                os.system("cp "+vbfnlo_in_path + " "+new_dir)
                os.system("cp "+anomV_in_path  + " "+new_dir)
                os.system("cp "+anomHVV_in_path+ " "+new_dir)
                os.system("cp "+cuts_in_path   + " "+new_dir)

                #change parameter in current param card
                changeParameter(input_path = new_dir, output_path = new_dir, file_name = file_name, name = name, value = value)


                start=time.time()
                #run VBFNLO at current dir
                os.system("/direct/usatlas+u/jennyz/VBFNLO/VBFNLO-2.7.1/bin/vbfnlo --input=. >> output.log")
                end = time.time()
                run_time = end-start
                with open("timer.txt", 'w') as file:
                    # read a list of lines into data
                    file.write(str(run_time))

                #the new dir becomes the old dir -- the path to copy the .dat files from
                previous_dir = new_dir

if __name__ =='__main__':
    parser=argparse.ArgumentParser(description='Change parameter in input .dat files of VBFNLO and run VBFNLO')
    parser.add_argument('-a', '--action',type=str, default = "", help='an action to be inserted in directory name')
    parser.add_argument('--param-path', type=str, default = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/myinput/VBFNLO/",
                        help='default .dat file import path')
    args=parser.parse_args()

    try:
        main(args.action,param_path=args.param_path)
    except KeyboardInterrupt:
        print('')
        ERROR('Exectution terminated.')
        INFO('Finished with error.')
        exit()
