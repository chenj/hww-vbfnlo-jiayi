#!/usr/bin/env python

#Created      Oct2, 2019 --Jiayi Chen, jennyz@brandeis.edu
#last update: Oct7, 2019 --Jiayi

import io, argparse, json, ast, os, time, collections
import numpy as np


#from Hannah
def buildJobFile( condor_name, path_to_script, previous_dir, new_dirs, path_to_good_grid):

    # Open the file for writing
    condor_file_name = '%s%scondor.sh' %(path_to_script,condor_name)
    print "INFO: creating", condor_file_name

    job = open( condor_file_name, 'w' )
    ##::: Add the contents for the Condor job configuration :::##
    #   Assign the executable
    job.write( 'Executable = {}submit_one_job.py \n'.format( path_to_script ) )
    #   Assign the output file
    job.write( 'Output = {}outCondor \n'.format( path_to_script ) )
    #   Assign the error file
    job.write( 'Error = {}errorCondor \n'.format( path_to_script ) )
    #   Assign the log file
    job.write( 'Log = {}logCondor \n'.format( path_to_script ) )
    job.write( 'accounting_group = group_atlas.bnl\n' )
    job.write( 'should_transfer_files = YES \n' )
    job.write( 'when_to_transfer_output = ON_EXIT\n')
    job.write( '\n' )
    #   Request additional CPU, diskspace, and memory
    #job.write( 'request_cpus = 2 \n' )
    #job.write( '+JobFlavour = "workday" \n')

    #   Add the condor queuing command
    job.write( 'Queue 1 Arguments from (\n' )
    for new_dir in new_dirs:
        job.write( "--dat-input-path %s --new-dir %s --path-to-grid %s\n"%(previous_dir,new_dir,path_to_good_grid ))
    job.write( ')' )
    return condor_file_name

def sendJob( jobfullpath ):
    cmd = 'condor_submit '
    args = jobfullpath
    os.system( cmd + args )

def main(args):

    condor_name = args.condor_name
    output_dir_label = args.output_label

    #path to VBFNLO_Analysis directory in spar machine
    VBFNLO_Analysis_path = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/VBFNLO_Analysis/"
    #VBFNLO_Analysis_path="/mnt/shared/Desktop/BNL/2019/HWW/HWW/"

    #path to a current new_parameters.json to use the parameters setting
    default_parameter_lists = VBFNLO_Analysis_path+"new_parameters2.json"#"new_parameters_remove_cuts.json"###

    #where to copy .dat files from
    datFiles_input_path = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/VBFNLO_Analysis/new_cuts_pts31_3/"
    #datFiles_input_path = "/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/VBFNLO_Analysis/wwjj_cW0cWWW0cB0_truth_cuts/"
    #datFiles_input_path="../new_cuts_pts31_3/"

    #full path to the NLO good grid
    #path_to_good_grid=datFiles_input_path+"grid3_1.out.2"
    path_to_good_grid="/usatlas/u/jennyz/VBFNLO/VBFNLO-2.7.1/VBFNLO_Analysis/new_cuts_pts31_3/grid3_1.out.2"
    #read new_parameters.json
    with open(default_parameter_lists, 'r') as file:
        #read a list of lines into data
        input_json = json.load(file)

    #translate json to dict
    input_json = ast.literal_eval(json.dumps(input_json))

    ########################################################################
    ##a list of parameters to change from the default new_parameters.json###
    param_card="vbfnlo.dat"
    #param_card = "cuts.dat"

    #PDF set number CT14nlo 0-56
    #param_name="NLO_PDFMEMBER"
    #new_values = np.arange(0,1,1)
    #param_name="NLO_PDFNAME"
    #new_values = np.arange(111,124,1)
    #for ind,value in enumerate(new_values):
    #new_values=["CT14nlo_as_0"+str(value) for value in new_values]
    input_json[param_card]["LO_PDFNAME"] = "NNPDF30_lo_as_0118"
    input_json[param_card]["NLO_PDFNAME"] = "NNPDF30_nlo_as_0118"
    input_json[param_card]["NLO_POINTS"] = "20"
    #input_json[param_card]["NLO_PDFNAME"] = "CT14nlo"
    #input_json[param_card]["LO_PDFNAME"] = "CT14lo"
    additional_param = {}
    #additional_param["NLO_PDFMEMBER"]=["15","19"]
    #additional_param["RJL_MIN"]=["0.1D0"]
    #additional_param["RLL_MIN"]=["0.01D0"]
    #additional_param["ETAJJ_MIN"]=["0.1D0"]
    #additional_param["DELY_JL"]=["0.05D0"]
    #PDF set number CT14nlo 0-56
    #additional_param["NLO_PDFNAME"] = ["NNPDF30_nlo_as_0117","NNPDF30_nlo_as_0119"]
    additional_param["NLO_PDFMEMBER"] = ["0"]#[str(i) for i in np.arange(0,101,1)]
    #additional_param["NLO_POINTS"] = ["30","31","32"]

    #additional_param["RJJ_MIN"] = ["0.4","2.0","3.0","4.0"]
    #new_values=[str(value)+"D0" for value in new_values]

    #QCD scales:
    vbfnlo_dat_file = "vbfnlo.dat"
    XIF_name="XIF"
    XIR_name="XIR"
    #XIF_new_values = ["0.5d0","1.0d0","2.0d0"]
    #XIR_new_values = ["0.5d0","1.0d0","2.0d0"]
    XIF_new_values = ["1"]
    XIR_new_values = ["1"]
    ########################################################################

    new_dirs=[]
    #iterate through all parameter names and their values
    for param_name in additional_param.keys():
        new_dir_prefix = output_dir_label #param_name + output_dir_label
        new_values = additional_param[param_name]

        #iterate throu all values, submit a condor job for each
        for value in new_values:

            #iterate through mu_F
            for xif in XIF_new_values:

                if "0.5" in xif:
                    muF="muF0.5"
                elif "2.0" in xif:
                    muF="muF2.0"
                elif "1.0" in xif:
                    muF="muF1.0"
                else:
                    muF=""

                #iterate through mu_R
                for xir in XIR_new_values:

                    if "0.5" in xir:
                        muR="muR0.5"
                    elif "2.0" in xir:
                        muR="muR2.0"
                    elif "1.0" in xir:
                        muR="muR1.0"
                    else:
                        muR=""

                    if "1.0" in xir and "1.0" in xif:
                        continue

                    os.chdir(VBFNLO_Analysis_path)

                    #re-initialize param_list back to default new_parameters.json
                    #print("param_list",parameter_list)
                    parameter_list = input_json.copy()

                    if param_card not in parameter_list.keys():
                        parameter_list[param_card] = {}
                    if vbfnlo_dat_file not in parameter_list.keys():
                        parameter_list[vbfnlo_dat_file] = {}

                    #get the new value and add to dict
                    parameter_list[param_card][param_name] = str(value)
                    parameter_list[vbfnlo_dat_file][XIF_name] = xif
                    parameter_list[vbfnlo_dat_file][XIR_name] = xir

                    #new_dir name CT14nlo-PDF-XX-muR0d5-muF2/ (XX=0,1,...56)
                    #new_dir = "CT14nlo-PDF-%s%s%s/" %(str(value),muR,muF)
                    new_dir = new_dir_prefix + "-%s%s%s/"%(value.strip("."),muR,muF)
                    new_dir = VBFNLO_Analysis_path + new_dir
                    new_dirs.append(new_dir)

                    #mk new_dir
                    os.mkdir(new_dir)
                    print "INFO: creaded new directory: ", new_dir
                    #cd new_dir
                    os.chdir(new_dir)

                    #write to new_parameters.json in the new_dir
                    new_parameters = "new_parameters.json"
                    with open(new_parameters,'w') as jFile:
                        json.dump(parameter_list ,jFile)

                    print "INFO: created new_parameters.json"

    #make condor.sh
    #condor_name = "RJJ_verytight_cuts_"
    condor_file_name = buildJobFile(condor_name,VBFNLO_Analysis_path, datFiles_input_path, new_dirs, path_to_good_grid)

    #send job
    sendJob( condor_file_name )


if __name__ =='__main__':
    parser=argparse.ArgumentParser(description='Change parameter in input .dat files of VBFNLO and run VBFNLO')
    #parser.add_argument('-a', '--action',type=str, default = "", help='an action to be inserted in directory name')
    parser.add_argument('--condor-name', type=str, help='condor_name_condor.sh')
    parser.add_argument('--output-label', type=str, help='output dir label')

    args=parser.parse_args()

    main(args)
