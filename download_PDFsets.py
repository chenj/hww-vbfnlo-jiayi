#!/usr/bin/env python
import os

#wget "http://lhapdfsets.web.cern.ch/lhapdfsets/current/CT14nlo_as_0113.tar.gz"
#-O- | tar xz -C /direct/usatlas+u/jennyz/local/share/LHAPDF
def download_and_untar(alpha_s):
    cmd = "wget \"%s%s%s\" %s %s" %(PDFset_prefix, alpha_s, tar, cmd_untar, path_to_PDFsets)
    print cmd
    #os.system(cmd)

PDFset_prefix="http://lhapdfsets.web.cern.ch/lhapdfsets/current/CT14nlo_as_"
tar = ".tar.gz"
cmd_untar = "-O- | tar xz -C"
path_to_PDFsets = "/direct/usatlas+u/jennyz/local/share/LHAPDF"

as_values = ["0114","0115","0116","0117","0118","0119","0120","0121","0122","0123"]

for alpha_s in as_values:
    download_and_untar(alpha_s)
